# nth.js

O nth.js, (pronunciado nat.js) é um grupo de funções que extendem o comportamento padrão do objeto Array em javascript.

Os métodos `Array.nth()` e `Array.nthmap()` adicionam a capacidade de seleção de elementos em arrays javascript usando uma notação inspirada pela pseudo-classe nth-child do css.

## Notação nth

A notação nth é inspirada na [pseudo-class css :nth-child](https://developer.mozilla.org/pt-BR/docs/Web/CSS/:nth-child).
O formato da notação é: &lt;nth&gt; = &lt;An-plus-B&gt; | even | odd

Onde:

> `a` indica um intervalo de repetição;
> `b` indica o elemento inicial

### Exemplos:

* 2n+1 - seleciona de 2 em 2 elementos a partir do índice 1, índices ímpares;
* 2n+0 - seleciona de 2 em 2 elementos a partir do índice 0, índices pares;
* 1n+5 - seleciona de 1 em 1 elemento a partir do índice 5;

## Array.nth()

O método `Array.nth` filtra elementos de um array baseado em uma notação nth.

### Sintaxe

```
Array.nth(notation)
```

#### Parâmetros

**notation**

> Uma notação nth válida

### Descrição

`Array.nth()` retorna um novo array contendo elementos filtrados segundo uma notação nth.

### Exemplos

```javascript
var numbers = [0,1,2,3,4,5,6,7,8,9,10]; // índices pares
console.log(numbers.nth(2n+0)) // console: [2,4,6,8,10]

// múltiplos de 3
console.log(numbers.nth(3n+0)) // console: [3,6,9]

var banana = "banana".split("");
console.log(banana.nth(2n+1)) // console: [a,a,a]

```

## Array.nthmap()

O método `Array.nth` recebe um objeto onde as chaves são notações nth e os valores das chaves são funções a serem aplicadas nos elementos selecionados.
Retorna uma cópia do vetor original com elementos manipulados.

### Sintaxe

```
Array.nthmap(notations)
```

#### Parâmetros

**notations**

> Um objeto javascript válido onde chaves são notações nth e seus valores são funçöes.

Exemplo:
```javascript

var notations = {
    "2n+0": function escrever_par(elemento){ return elemento + "é par"},
    "2n+1": function escrever_impar(elemento){ return elemento + "é ímpar"}
};

```

### Descrição

`Array.nthmap()` retorna uma cópia de um array depois de aplicar nesse array uma série de operações.

### Exemplos

```javascript
var numbers = [0,1,2,3,4]; // índices pares
var notations = {
    "2n+0": function escrever_par(elemento){ return elemento + "é par"},
    "2n+1": function escrever_impar(elemento){ return elemento + "é ímpar"}
};
console.log(numbers.nth(notations)) // console: ["0 é par", "2 é ímpar", "3 é par", "4 é ímpar"]

```

## Licença

MIT - Copyright (c) 2017 Teles

## Veja também

* [ArrayMixer](http://jotateles.com.br/array-mixer/) é um utilitário javascript de menos de 1kb capaz de organizar grupos de arrays de forma personalizada.