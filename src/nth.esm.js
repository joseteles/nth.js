if (typeof Array.prototype.nth !== "function") {
    Array.prototype.nth = function nth(notation){
        let [gap, initial] = notation.split("n+").map(part => parseInt(part));

        return this.filter(function nth_filter(item, index) {
            return index === initial || (index > initial && (index - initial) % gap  === 0 );
        });
    };
}

if (typeof Array.prototype.nthmap !== "function") {

    Array.prototype.nthmap = function nth(notations){
        let self = this;

        Object.keys(notations).forEach(notation => {
            let [gap, initial] = notation.split("n+").map(part => parseInt(part));
            let callback = notations[notation];

            self = self.map(function nth_filter(item, index) {
                let has_match_notation = index === initial || (index > initial && (index - initial) % gap  === 0 );
                return has_match_notation ? callback(item) : item;
            });
        });
        return self;
    };
}

